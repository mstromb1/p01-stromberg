//
//  ViewController.swift
//  HelloWorld
//
//  Created by Matthew Stromberg on 2/1/16.
//  Copyright © 2016 Matthew Stromberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var mySlider: UISlider!
    @IBOutlet weak var numberLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func myButton(sender: AnyObject) {
        myLabel.text = "Label changed!";
    }
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        var currentValue = Int(sender.value)
        
        numberLabel.text = "\(currentValue)"
    }

}

